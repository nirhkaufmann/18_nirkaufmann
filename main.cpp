#include "Helper.h"
#include <iostream>
#include <Windows.h>
#include <vector>

using namespace std;

void cmd();
string pwd();
void cd(string s);
void create(string name);
void ls();
int main()
{
	
	cmd();
	system("pause");
	return 0;
}

void cmd()
{
	vector<string> commands;
	string in = "";
	while (true)
	{
		cout << pwd()<<">>";
		getline(cin, in);
		Helper::trim(in);
		commands = Helper::get_words(in);
		if (commands[0].compare("pwd") == 0) cout<<pwd()<<endl;
		else if (commands[0].compare("cd") == 0) cd(commands[1]);
		else if (commands[0].compare("create") == 0) create(commands[1]);
		else if (commands[0].compare("ls") == 0) ls();


	}
}

string pwd()
{
	TCHAR buff[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, buff);
	string s = string(buff);
	return s;

}

void cd(string path)
{
	SetCurrentDirectory(path.c_str());
}


void create(string name)
{
	HANDLE file = CreateFile(name.c_str(), GENERIC_READ, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	CloseHandle(file);
}

void ls()
{
	string path = pwd();
	path += "\\*";
	WIN32_FIND_DATA ffd;
	HANDLE hfind = FindFirstFile(path.c_str(), &ffd);
	if (hfind != INVALID_HANDLE_VALUE)
	{
		do
		{
			cout <<endl<< ffd.cFileName;
		} while (FindNextFile(hfind, &ffd)!=0);
		FindClose(hfind);
	}

}